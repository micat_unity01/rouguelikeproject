using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public struct SettingData
{
    // 창모드
    public FullScreenMode screenMode;

    // 설정된 해상도
    public int width;
    public int height;

    // refresh rate
    public uint numerator;
    public uint denominator;

}
