using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// NPC 의 정보를 나타내기 위한 구조체입니다.
/// </summary>
[System.Serializable]
public struct NpcData
{
    /// <summary>
    /// NPC 코드
    /// </summary>
    public string npcCode;

    // NPC 이름
    public string npcName;

    // 대화에 사용되는 UI
    public NpcUI useNpcDialog;

    // 기본 대화 내용
    [Multiline]
    public string defaultDialog;
}
