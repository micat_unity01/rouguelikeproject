using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EnemyDataElem
{
    public string m_EnemyCode;
    public EnemyData m_EnemyData;
}

[System.Serializable]
public struct EnemyData
{
    [Header("# 적 이름")]
    public string enemyName;

    [Header("# 적 최대 체력")]
    public float maxHp;

    [Header("# 적 공격력")]
    public float atk;

}


