using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 캐릭터 정보를 나타내기 위한 구조체
/// </summary>
public struct CharacterData
{
    /// <summary>
    /// 최대 체력
    /// </summary>
    public float maxHp;

    /// <summary>
    /// 최대 스테미너
    /// </summary>
    public float maxStamina;

    /// <summary>
    /// 공격력
    /// </summary>
    public float atk;

    /// <summary>
    /// 이동 속도
    /// </summary>
    public float maxSpeed;

    /// <summary>
    /// 기본 정보로 초기화합니다.
    /// </summary>
    public static CharacterData Initialize()
    {
        CharacterData newData = new();
        newData.maxHp = 100.0f;
        newData.maxStamina = 50.0f;
        newData.atk = 20.0f;
        newData.maxSpeed = 6.0f;
        return newData;
    }
}
