using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class RadioButtonGroup : MonoBehaviour
{
    [Header("# 선택된 라디오 버튼 인덱스")]
    public int m_SelectedRadioButtonIndex = 0;

    public List<RadioButton> radioButtons { get; private set; }



    /// <summary>
    /// 라디오 버튼 선택됨 이벤트
    /// </summary>
    public event System.Action<RadioButton /*selectedRadioButton*/, int/*index*/> onRadioButtonSelected;

    private void Awake()
    {
        if (radioButtons == null)
        {
            InitializeRadioButtons();
        }
    }

    public void InitializeRadioButtons()
    {
        radioButtons = new();

        // 하위에 생성된 라디오 버튼들을 찾습니다.
        foreach (RadioButton findRadioButton in GetComponentsInChildren<RadioButton>())
        {
            RegisterRadioButton(findRadioButton);
        }

        radioButtons[m_SelectedRadioButtonIndex].OnRadioButtonSelected();
    }

    public void RegisterRadioButton(RadioButton radioButton)
    {
        radioButtons.Add(radioButton);
        radioButton.OnRadioButtonRegistered(this);
    }

    /// <summary>
    /// 라디오 버튼 선택 요청
    /// </summary>
    /// <param name="radioButton">라디오 버튼 객체를 전달합니다.</param>
    public void RequestRadioButtonSelect(RadioButton radioButton)
    {
        // 매개 변수로 전달된 라디오 버튼 객체를 리스트 내에서 찾아 인덱스를 얻습니다.
        int index = radioButtons.FindLastIndex((elem) => elem == radioButton);

        // 전달된 라디오 버튼을 선택 요청시킵니다.
        RequestRadioButtonSelect(index);
    }

    /// <summary>
    /// 라디오 버튼 선택 요청
    /// </summary>
    /// <param name="radioIndex">선택시킬 라디오 버튼 인덱스를 전달합니다.</param>
    public void RequestRadioButtonSelect(int radioIndex)
    {
        // 범위를 벗어난 경우 함수 호출 종료.
        if (radioButtons.Count <= radioIndex) return;


        // 다른 인덱스가 설정된 경우
        if (m_SelectedRadioButtonIndex != radioIndex)
        {
            RadioButton selectedRadioButton = radioButtons[radioIndex];

            // 기존에 선택되었던 라디오 버튼 선택 해제
            radioButtons[m_SelectedRadioButtonIndex].OnRadioButtonUnselected();

            // 선택된 라디오 버튼 인덱스 설정
            m_SelectedRadioButtonIndex = radioIndex;

            // 새롭게 선택되는 라디오 버튼 선택
            selectedRadioButton.OnRadioButtonSelected();

            // 라디오 버튼 선택됨 이벤트 발생
            onRadioButtonSelected?.Invoke(selectedRadioButton, radioIndex);
        }
    }
}
