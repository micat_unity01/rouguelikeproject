using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadioButton : MonoBehaviour
{
    /// <summary>
    /// 해당 라디오 버튼을 소유하는 라디오 버튼 그룹
    /// </summary>
    private RadioButtonGroup _OwnerGroup;

    protected RadioButtonGroup ownerGroup => _OwnerGroup;

    /// <summary>
    /// 라디오 버튼이 그룹에 등록될 때 호출되는 메서드
    /// </summary>
    /// <param name="ownerGroup"></param>
    public virtual void OnRadioButtonRegistered(RadioButtonGroup ownerGroup)
    {
        _OwnerGroup = ownerGroup;
    }

    /// <summary>
    /// 라디오 버튼이 선택되었을 때 호출되는 메서드입니다.
    /// </summary>
    public virtual void OnRadioButtonSelected()
    {

    }

    /// <summary>
    /// 라디오 버튼이 선택 해제되었을 경우 호출됩니다.
    /// </summary>
    public virtual void OnRadioButtonUnselected()
    {

    }




}
