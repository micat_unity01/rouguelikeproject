using startup.single;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class NpcUI : MonoBehaviour
{
    [Header("# 텍스트")]
    public TMP_Text m_NameText;
    public TMP_Text m_DialogText;

    [Header("# 버튼")]
    public Button m_CloseDialogButton;

    /// <summary>
    /// NPC UI 가 닫힐 때 발생하는 이벤트입니다.
    /// </summary>
    public event System.Action onNpcUIClosed;

    public virtual void InitializeNpcUI(in NpcData npcData)
    {
        m_NameText.text = npcData.npcName;
        m_DialogText.text = npcData.defaultDialog;

        // 닫기 버튼에 이벤트 바인딩
        m_CloseDialogButton.onClick.AddListener(OnCloseButtonClicked);
    }

    private void OnCloseButtonClicked()
    {
        GameSceneInstance sceneInstance = SceneManagerBase.instance.GetSceneInstance<GameSceneInstance>();

        // 게임 플레이 입력 모드로 설정
        sceneInstance.playerController.SetControlMode(Constants.ACTIONMAP_GAMEPLAY_MODE);

        // NPC UI 닫힘 이벤트 발생
        onNpcUIClosed?.Invoke();

        // NPC UI 제거
        Destroy(gameObject);
    }



}
