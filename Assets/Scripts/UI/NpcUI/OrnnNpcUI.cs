using startup.single;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public sealed class OrnnNpcUI : NpcUI
{
    public Button m_UpgradeMaxHpButton;
    public Button m_UpgradeMaxStaminaButton;
    public Button m_UpgradeMaxSpeedButton;
    public Button m_UpgradeAtkButton;

    public override void InitializeNpcUI(in NpcData npcData)
    {
        base.InitializeNpcUI(npcData);

        // Bind Button Events
        m_UpgradeMaxHpButton.onClick.AddListener(OnUpgradeMaxHpButtonClicked);
        m_UpgradeMaxStaminaButton.onClick.AddListener(OnUpgradeMaxStaminaButtonClicked);
        m_UpgradeMaxSpeedButton.onClick.AddListener(OnUpgradeMaxSpeedButtonClicked);
        m_UpgradeAtkButton.onClick.AddListener(OnUpgradeAtkButtonClicked);

    }


    private void UpdateCharacterData(in CharacterData characterData)
    {
        GameSceneInstance sceneInst =
            SceneManagerBase.instance.GetSceneInstance<GameSceneInstance>();

        // 변경된 CharacterData 를 LocalDataManager 에 적용
        LocalDataManager.instance.characterData = characterData;

        // 내용을 캐릭터 객체에 적용시킵니다.
        sceneInst.playerController.playerCharacter.OnCharacterDataUpdated();
    }

    private void OnUpgradeMaxHpButtonClicked()
    {
        CharacterData characterData = LocalDataManager.instance.characterData;
        characterData.maxHp += 10.0f;
        UpdateCharacterData(characterData);
    }


    private void OnUpgradeMaxStaminaButtonClicked()
    {
        CharacterData characterData = LocalDataManager.instance.characterData;
        characterData.maxStamina += 20.0f;
        UpdateCharacterData(characterData);
    }

    private void OnUpgradeMaxSpeedButtonClicked()
    {
        CharacterData characterData = LocalDataManager.instance.characterData;
        characterData.maxSpeed += 0.5f;
        UpdateCharacterData(characterData);
    }

    private void OnUpgradeAtkButtonClicked()
    {
        CharacterData characterData = LocalDataManager.instance.characterData;
        characterData.atk += 10.0f;
        UpdateCharacterData(characterData);
    }



}
