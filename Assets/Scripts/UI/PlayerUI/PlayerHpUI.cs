using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHpUI : MonoBehaviour
{
    [Header("# Hp UI")]
    public Image m_HpValueImage;
    public Image m_HpValueFollowImage;
    public TMP_Text m_HpValueText;

    private float _MaxHp;
    private float _CurrentHp;

    /// <summary>
    /// 체력이 변경될 때마다 호출되는 메서드입니다.
    /// </summary>
    /// <param name="newHp"></param>
    public void OnHpValueChanged(float newHp)
    {
        _CurrentHp = newHp;
        float fill = _CurrentHp / _MaxHp;
        m_HpValueImage.fillAmount = fill;

        // 텍스트 설정
        m_HpValueText.text = $"{_CurrentHp} / {_MaxHp}";
    }

    public void InitializePlayerHpUI(float maxHp)
    {
        _CurrentHp = _MaxHp = maxHp;

        OnHpValueChanged(_CurrentHp);

        m_HpValueFollowImage.fillAmount = m_HpValueImage.fillAmount;
    }

    public void SetMaxHp(float maxHp)
    {
        // 현재 체력을 얻습니다.
        float currentHp = m_HpValueImage.fillAmount * _MaxHp;

        _MaxHp = maxHp;

        OnHpValueChanged(currentHp);
    }


    private void Update()
    {
        UpdateHpValueFollowFillAmount();
    }

    private void UpdateHpValueFollowFillAmount()
    {
        float targetFill = m_HpValueImage.fillAmount;
        float currentFill = m_HpValueFollowImage.fillAmount;
        m_HpValueFollowImage.fillAmount = Mathf.Lerp(currentFill, targetFill, 0.1f);
    }

}
