using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class NpcHUD : HUDBase
{
    [Header("# NPC 이름")]
    public TMP_Text m_NameText;

    List<Graphic> m_HUDGraphics;

    public void InitializeNpcHUD(string npcName)
    {
        m_NameText.text = npcName;

        // Graphic 의 하위 클래스 객체들을 모두 얻습니다.
        m_HUDGraphics = new();
        foreach (Graphic graphic in GetComponentsInChildren<Graphic>())
        {
            m_HUDGraphics.Add(graphic);
        }
    }

    protected override void OnHUDHide()
    {
        base.OnHUDHide();


        foreach (Graphic graphic in m_HUDGraphics)
        {
            Color currentColor = graphic.color;
            currentColor.a = 0.0f;
            graphic.color = currentColor;
        }
    }

    protected override void OnHUDShow()
    {
        base.OnHUDShow();
        foreach (Graphic graphic in m_HUDGraphics)
        {
            Color currentColor = graphic.color;
            currentColor.a = 1.0f;
            graphic.color = currentColor;
        }
    }
}
