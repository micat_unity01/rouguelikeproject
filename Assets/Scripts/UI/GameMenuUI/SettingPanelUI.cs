using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SettingPanelUI : MonoBehaviour
{
    [Header("# 해상도")]
    public TMP_Dropdown m_ResolutionDropdown;

    [Header("# 창모드 라디오 버튼 그룹")]
    public RadioButtonGroup m_WindowModeRadioGroup;

    [Header("# 버튼")]
    public Button m_CloseButton;
    public Button m_ApplyButton;

    private List<Resolution> _Resolutions = new();

    private SettingData _SettingData;

    /// <summary>
    /// 설정 패널이 닫힐 때 발생하는 이벤트
    /// </summary>
    public event System.Action onSettingPanelClosed;

    private void Awake()
    {
        _SettingData = LocalDataManager.instance.settingData;

        InitializeWindowModeRadioGroup();
        InitializeResolutionDropDown();
        m_CloseButton.onClick.AddListener(OnCloseButtonClicked);
        m_ApplyButton.onClick.AddListener(OnApplyButtonClicked);
    }

    private void InitializeWindowModeRadioGroup()
    {
        if (m_WindowModeRadioGroup.radioButtons == null)
            m_WindowModeRadioGroup.InitializeRadioButtons();


        for (int i = 0; i < m_WindowModeRadioGroup.radioButtons.Count; ++i)
        { 
            // 창모드 라디오 버튼
            SettingWindowModeRadioButton windowModeRadioButton = 
                m_WindowModeRadioGroup.radioButtons[i] as SettingWindowModeRadioButton;

            if (windowModeRadioButton.m_ScreenMode == _SettingData.screenMode)
            {
                m_WindowModeRadioGroup.RequestRadioButtonSelect(i);
                break;
            }
        }


        // 라디오 버튼 선택 이벤트 설정
        m_WindowModeRadioGroup.onRadioButtonSelected += OnWindowModeChanged;
    }

    private void InitializeResolutionDropDown()
    {
        // 기본 옵션 제거
        m_ResolutionDropdown.options.Clear();

        // 옵션 추가
        foreach (Resolution resolution in Screen.resolutions)
        {
            if (resolution.width > 600)
            {
                // 사용 가능한 해상도로 추가합니다.
                _Resolutions.Add(resolution);

                string resolutionOption = $"[{resolution.width} X {resolution.height}] {resolution.refreshRateRatio}Hz";
                m_ResolutionDropdown.options.Add(new(resolutionOption));

                // 현재 옵션을 찾아 설정합니다.
                if (resolution.width == _SettingData.width &&
                    resolution.height == _SettingData.height)
                {
                    m_ResolutionDropdown.value = _Resolutions.Count - 1;
                }
            }
        }

        m_ResolutionDropdown.onValueChanged.AddListener(OnResolutionSelected);
    }

    private void OnResolutionSelected(int index)
    {
        // 현재 선택된 해상도
        Resolution selectedResolution = _Resolutions[index];

        // 설정된 너비, 높이를 얻습니다.
        int width = selectedResolution.width;
        int height = selectedResolution.height;

        // 해상도를 갱신합니다.
        UpdateResolution(width, height, selectedResolution.refreshRateRatio);
    }

    private void OnWindowModeChanged(RadioButton radioButton, int selectedIndex)
    {
        SettingWindowModeRadioButton selectedRadioButton = (radioButton as SettingWindowModeRadioButton);

        UpdateScreenMode(selectedRadioButton.m_ScreenMode);
    }


    private void OnCloseButtonClicked()
    {
        onSettingPanelClosed?.Invoke();
        Destroy(gameObject);
    }

    private void OnApplyButtonClicked()
    {
        LocalDataManager.instance.settingData = _SettingData;
        LocalDataManager.instance.WriteSettingData();
        OnCloseButtonClicked();
    }

    public void UpdateScreenMode(FullScreenMode newScreenMode)
    {
        _SettingData.screenMode = newScreenMode;
    }

    public void UpdateResolution(int newWidth, int newHeight, RefreshRate refreshRate)
    {
        _SettingData.width = newWidth;
        _SettingData.height = newHeight;
        _SettingData.numerator = refreshRate.numerator;
        _SettingData.denominator = refreshRate.denominator;
    }



}
