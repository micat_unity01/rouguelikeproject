using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public sealed class SettingWindowModeRadioButton : RadioButton
{
    [Header("# 선택 표시 이미지")]
    public Image m_SelectedImage;

    [Header("# 옵션")]
    public FullScreenMode m_ScreenMode;

    /// <summary>
    /// 버튼 컴포넌트
    /// </summary>
    private Button _RadioButton;

    /// <summary>
    /// 라디오 버튼 텍스트
    /// </summary>
    private TMP_Text _RadioButtonText;

    /// <summary>
    /// 선택됨 여부를 나타냅니다.
    /// </summary>
    private bool _IsSelected;

    public string label => _RadioButtonText.text;

    private void Awake()
    {
        // 버튼 컴포넌트를 찾습니다.
        _RadioButton = GetComponent<Button>();

        // 텍스트 컴포넌트를 찾습니다.
        _RadioButtonText = GetComponentInChildren<TMP_Text>();

        // 버튼 클릭 이벤트 설정
        _RadioButton.onClick.AddListener(OnRadioButtonClicked);

        // 이미지 숨김
        m_SelectedImage.enabled = false;
    }

    private void Start()
    {
        UpdateSelectImage();
    }

    /// <summary>
    /// 버튼이 클릭되었을 경우 호출되는 메서드입니다.
    /// </summary>
    private void OnRadioButtonClicked()
    {
        // 자신을 선택 요청시킵니다.
        ownerGroup.RequestRadioButtonSelect(this);
    }

    public override void OnRadioButtonSelected()
    {
        base.OnRadioButtonSelected();

        _IsSelected = true;
        UpdateSelectImage();
    }

    public override void OnRadioButtonUnselected()
    {
        base.OnRadioButtonUnselected();

        _IsSelected = false;
        UpdateSelectImage();
    }

    public void UpdateSelectImage()
    {
        m_SelectedImage.enabled = _IsSelected;
    }




}
