using startup.single;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameMenuUI : MonoBehaviour
{
    [Header("# 버튼")]
    public Button m_ResumeButton;
    public Button m_SettingButton;
    public Button m_ExitButton;

    [Header("# 설정 패널 프리팹")]
    public SettingPanelUI m_SettingPanelPrefab;

    private SettingPanelUI _SettingPanel;

    private void Awake()
    {
        m_ResumeButton.onClick.AddListener(OnResumeButtonClicked);
        m_SettingButton.onClick.AddListener(OnSettingButtonClicked);
        m_ExitButton.onClick.AddListener(OnExitButtonClicked);
    }

    private void OnResumeButtonClicked()
    {
        // GamePlay 입력 모드로 전환
        SceneManagerBase.instance.GetSceneInstance<GameSceneInstance>().playerController.SetControlMode(
            Constants.ACTIONMAP_GAMEPLAY_MODE);

        // 게임 재개
        Time.timeScale = 1.0f;

        // UI 제거
        Destroy(gameObject);
    }

    private void OnSettingButtonClicked()
    {
        _SettingPanel = Instantiate(m_SettingPanelPrefab, transform);
        _SettingPanel.onSettingPanelClosed += () => _SettingPanel = null;
    }

    private void OnExitButtonClicked()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;

//#elif UNITY_STANDALONE
#elif UNITY_STANDALONE_WIN
        Application.Quit();
#endif
    }

    public void InitializeGameMenuUI()
    {
        // 게임 일시 중단
        Time.timeScale = 0.0f;
    }

}
