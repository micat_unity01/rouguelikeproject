using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelBounds : MonoBehaviour
{
    public string m_SubLevelName;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == Constants.TAG_PLAYER) 
            StartCoroutine(LoadSubLevel());
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == Constants.TAG_PLAYER) 
            StartCoroutine(StartUnloadScene());
    }

    private IEnumerator LoadSubLevel()
    {
        Debug.Log($"서브 레벨 로드 시작! [{m_SubLevelName}]");
        // 로딩이 끝날 때까지 대기합니다.
        yield return SceneManager.LoadSceneAsync(m_SubLevelName, LoadSceneMode.Additive);
        Debug.Log($"서브 레벨 로드 끝! [{m_SubLevelName}]");
    }

    private IEnumerator StartUnloadScene()
    {
        //AsyncOperation ao = SceneManager.UnloadSceneAsync(m_SubLevelName);

        // 작업이 끝날 때까지 대기합니다.
        //yield return new WaitUntil(() => ao.isDone);

        Debug.Log($"서브 레벨 언로드 시작! [{m_SubLevelName}]");
        yield return SceneManager.UnloadSceneAsync(m_SubLevelName);
        Debug.Log($"서브 레벨 언로드 끝! [{m_SubLevelName}]");
    }

}
