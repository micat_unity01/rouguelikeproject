using startup.single;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerCharacterAttack : MonoBehaviour
{
    private static AttackDataScriptableObject _AttackDataScriptableObject;

    [Header("# 무기")]
    public Transform m_WeaponTransform;

    [Header("# 소켓")]
    public Transform m_WeaponBackSocket;
    public Transform m_WeaponHandSocket;
    public Transform m_WeaponAttackAreaStart;
    public Transform m_WeaponAttackAreaEnd;

    [Header("# 적 레이어")]
    public LayerMask m_EnemyLayer;

    /// <summary>
    /// 플레이어 캐릭터를 나타냅니다.
    /// </summary>
    private PlayerCharacter _PlayerCharacter;

    /// <summary>
    /// 현재까지 진행된 콤보 카운트
    /// </summary>
    private int _CurrentComboCount;

    /// <summary>
    /// 목표 콤보 카운트
    /// </summary>
    private int _TargetComboCount;

    /// <summary>
    /// 다음 공격 입력을 확인하는 상태임을 나타냅니다.
    /// </summary>
    private bool _IsNextAttackChecking = true;

    /// <summary>
    /// 공격중임을 나타냅니다.
    /// </summary>
    private bool _IsAttack;

    /// <summary>
    /// 현재 진행중인 공격 대미지
    /// </summary>
    private float _CurrentAttackDamage;

    /// <summary>
    /// 이전에 진행했던 공격 이름입니다.
    /// </summary>
    private string _PrevAttackName;

    /// <summary>
    /// 현재 진행중인 공격 데이터입니다.
    /// </summary>
    private AttackData? _CurrentAttackData;

    /// <summary>
    /// 요청된 공격 데이터들을 담아둘 큐
    /// </summary>
    private Queue<AttackData> _RequestedAttackDatas = new();

    /// <summary>
    /// 공격 시 감지한 적 객체들을 잠시 저장해둘 리스트
    /// </summary>
    private List<EnemyCharacter> _AttackAreaDetectedEnemies = new();

    /// <summary>
    /// 공격 영역 활성화 상태를 나타냅니다.
    /// </summary>
    private bool _AttackAreaEnable;

    /// <summary>
    /// 무기 장착 상태를 나타내는 프로퍼티입니다.
    /// </summary>
    public bool isWeaponEquipped { get; private set; }

    /// <summary>
    /// 현재 콤보 카운트에 대한 읽기 전용 프로퍼티
    /// </summary>
    public int currentComboCount => _CurrentComboCount;

    /// <summary>
    /// 공격중 상태에 대한 읽기 전용 프로퍼티
    /// </summary>
    public bool isAttack => _IsAttack;

    /// <summary>
    /// 방어 상태에 대한 프로퍼티입니다.
    /// </summary>
    public bool isBlocked { get; private set; }

    /// <summary>
    /// 공격력
    /// </summary>
    public float atk { get; set; }

    /// <summary>
    /// 공격 애니메이션 재생 요청시 호출됩니다.
    /// </summary>
    public event System.Action<string /* attackName*/> onAttackAnimationRequest;

    /// <summary>
    /// 공격 시작 시 발생하는 이벤트
    /// </summary>
    public event System.Action onAttackStarted;

    /// <summary>
    /// 공격이 끝났을 경우 발생하는 이벤트
    /// </summary>
    public event System.Action onAttackFinished;

    /// <summary>
    /// 방어 시작 시 발생되는 이벤트
    /// </summary>
    public event System.Action onBlockStarted;

    /// <summary>
    /// 방어가 끝날 때 발생되는 이벤트
    /// </summary>
    public event System.Action onBlockFinished;

    #region DEBUG
    private DrawGizmoSpheresInfo _AttackAreaDrawGizmoInfo;
    #endregion

    private void Awake()
    {
        _PlayerCharacter = GetComponent<PlayerCharacter>();

        if (_AttackDataScriptableObject == null)
        {
            _AttackDataScriptableObject =
                Resources.Load<AttackDataScriptableObject>("ScriptableObject/AttackData");
        }
    }

    private void Start()
    {
        // 무기를 등에 배치합니다.
        WeaponToBack();
    }

    private void Update()
    {
        AttackProcedure();
    }

    private void FixedUpdate()
    {
        if (_AttackAreaEnable)
        {
            if (CheckAttackArea(out RaycastHit[] hitResults))
            {
                ApplyDamage(hitResults);
            }
        }
    }

    private void AttackProcedure()
    {
        // 만약 이미 진행중인 공격이 존재한다면 실행 취소
        if (_CurrentAttackData != null) return;

        // 요청된 공격이 존재하지 않는다면 실행 취소.
        if (_RequestedAttackDatas.Count == 0) return;


        // 사용할 공격 데이터를 얻습니다.
        _CurrentAttackData = _RequestedAttackDatas.Dequeue();

        // 사용될 공격 대미지를 기록합니다.
        _CurrentAttackDamage = _CurrentAttackData.Value.attackDamages[_CurrentComboCount];

        _AttackAreaDetectedEnemies.Clear();

        // 공격중 상태로 설정합니다.
        _IsAttack = true;

        // 공격 시작 이벤트 발생
        onAttackStarted?.Invoke();

        int animNameIndex = _CurrentComboCount;
        string animName = _CurrentAttackData.Value.animationNames[animNameIndex];

        ++_CurrentComboCount;

        onAttackAnimationRequest?.Invoke(animName);
    }

    private void WeaponToBack()
    {
        // 무기를 등에 배치합니다.
        m_WeaponTransform.SetParent(m_WeaponBackSocket);
        m_WeaponTransform.localPosition = Vector3.zero;
        m_WeaponTransform.localRotation = Quaternion.identity;
        m_WeaponTransform.localScale = Vector3.one;
    }

    private void WeaponToHand()
    {
        // 무기를 손에 배치합니다.
        m_WeaponTransform.SetParent(m_WeaponHandSocket);
        m_WeaponTransform.localPosition = Vector3.zero;
        m_WeaponTransform.localRotation = Quaternion.identity;
        m_WeaponTransform.localScale = Vector3.one;
    }

    /// <summary>
    /// 공격 영역을 검사합니다.
    /// </summary>
    /// <param name="hitResults">감지 결과를 반환받을 변수를 전달합니다.</param>
    /// <returns>감지 결과가 존재함을 반환합니다.</returns>
    private bool CheckAttackArea(out RaycastHit[] hitResults)
    {
        Vector3 checkDirection =
            (m_WeaponAttackAreaEnd.position - m_WeaponAttackAreaStart.position).normalized;

        float maxDistance = Vector3.Distance(
            m_WeaponAttackAreaEnd.position, m_WeaponAttackAreaStart.position);

        Ray ray = new Ray(m_WeaponAttackAreaStart.position, checkDirection);

        bool isHit = PhysicsExt.SphereCastAll(
            out _AttackAreaDrawGizmoInfo,
            ray,
            out hitResults,
            0.3f,
            maxDistance,
            m_EnemyLayer,
            QueryTriggerInteraction.UseGlobal);

        return isHit;
    }

    /// <summary>
    /// 대미지를 적용합니다.
    /// </summary>
    /// <param name="hitResults"></param>
    private void ApplyDamage(RaycastHit[] hitResults)
    {
        GameSceneInstance sceneInstance = SceneManagerBase.instance.GetSceneInstance<GameSceneInstance>();

        foreach(RaycastHit hitResult in hitResults)
        {
            EnemyCharacter enemy = sceneInstance.GetEnemyCharacterFromCollider(hitResult.collider);

            if (!_AttackAreaDetectedEnemies.Contains(enemy))
            {
                _AttackAreaDetectedEnemies.Add(enemy);

                // 공격력 연산
                enemy.OnDamaged(_PlayerCharacter, atk * _CurrentAttackDamage);
            }
        }

    }

    /// <summary>
    /// 무기 장착 지역에 입장한 경우 호출됩니다.
    /// </summary>
    public void OnEquipWeaponZoneEnter()
    {
        isWeaponEquipped = true;

        // 무기를 손에 배치합니다.
        WeaponToHand();
    }

    /// <summary>
    /// 무기 장착 해제 지역에 입장한 경우 호출됩니다.
    /// </summary>
    public void OnUnEquipWeaponZoneEnter()
    {
        isWeaponEquipped = false;

        // 무기를 등에 배치합니다.
        WeaponToBack();
    }

    public void RequestAttack(string attackName)
    {
        if (!isWeaponEquipped) return;

        if (_IsNextAttackChecking)
        {
            AttackDataElem attackDataElem = _AttackDataScriptableObject.GetAttackDataFromName(attackName);

            // 공격 데이터를 찾지 못한 경우
            if (attackDataElem == null)
            {
                Debug.LogError($"공격 데이터를 찾지 못했습니다. (attackName : {attackName})");
                throw new System.Exception();
            }

            // 공격 데이터를 얻습니다.
            AttackData attackData = attackDataElem.m_AttackData;

            // 같은 공격을 진행하는 경우
            if (_PrevAttackName == attackName)
            {
                if (attackData.maxComboCount <= _TargetComboCount)
                {
                    return;
                }
            }


            // 요청된 공격을 큐에 넣습니다.
            _RequestedAttackDatas.Enqueue(attackData);

            // 이전 공격 이름을 기록합니다.
            _PrevAttackName = attackName;

            // 목표 콤보를 증가시킵니다.
            if (attackData.maxComboCount > _TargetComboCount)
            {
                ++_TargetComboCount;
            }
        }
    }

    public void RequestBlock()
    {
        // 공격중이 아닌 경우만 실행합니다.
        if (_IsAttack) return;

        // 땅에 있을 경우에만 실행합니다.
        if (!_PlayerCharacter.movementComponent.isGrounded) return;

        // 공격 가능한 상태에서만 실행합니다.
        if (!isWeaponEquipped) return;

        isBlocked = true;
        onBlockStarted?.Invoke();
    }

    public void FinishBlock()
    {
        if (isBlocked)
        {
            isBlocked = false;
            onBlockFinished?.Invoke();
        }

    }

    /// <summary>
    /// 콤보 공격이 있는 경우 다음 공격 입력을 대기하기 시작합니다.
    /// </summary>
    public void StartNextAttackCheck()
    {
        _IsNextAttackChecking = true;
    }

    /// <summary>
    /// 콤보 공격 입력 대기를 멈춥니다.
    /// </summary>
    public void FinishNextAttackCheck()
    {
        _IsNextAttackChecking = false;
    }

    public void ClearNextAttack()
    {
        _CurrentComboCount = _TargetComboCount = 0;
    }

    /// <summary>
    /// 하나의 공격이 끝났을 경우 호출될 메서드입니다.
    /// </summary>
    public void OnAttackSequenceFinished()
    {
        // 다음으로 진행될 공격이 존재하지 않는 경우
        if (_CurrentComboCount == _TargetComboCount)
        {
            // 콤보 카운트 초기화
            _CurrentComboCount = _TargetComboCount = 0;
            _CurrentAttackData = null;

            _IsAttack = false;

            onAttackFinished?.Invoke();

            // 다음 공격 입력을 확인할 수 있도록 합니다.
            StartNextAttackCheck();
        }
        // 다음 공격이 존재하는 경우
        else
        {
            _CurrentAttackData = null;
        }
    }

    /// <summary>
    /// 공격 영역 활성화 상태를 설정합니다.
    /// </summary>
    /// <param name="enable">설정시킬 활성화 상태를 전달합니다.</param>
    public void SetEnableAttackArea(bool enable)
    {
        _AttackAreaEnable = enable;
    }


#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if (_AttackAreaEnable)
        {
            PhysicsExt.DrawGizmoSpheres(in _AttackAreaDrawGizmoInfo);
        }
    }
#endif



}
