using startup.single;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    private PlayerInput _PlayerInput;

    /// <summary>
    /// 플레이어 UI 객체를 나타냅니다.
    /// </summary>
    public PlayerUI playerUI { get; private set; }

    /// <summary>
    /// 조종하는 캐릭터에 대한 프로퍼티
    /// </summary>
    public PlayerCharacter playerCharacter { get; private set; }





    private void Awake()
    {
        // Get SceneInstance
        GameSceneInstance sceneInstance = SceneManagerBase.instance.GetSceneInstance<GameSceneInstance>();

        // Get Player UI
        playerUI = sceneInstance.m_UsePlayerUI;

        // Get PlayerInput Component
        _PlayerInput = GetComponent<PlayerInput>();

        // 현재 입력 모드 설정
        SetControlMode(_PlayerInput.currentActionMap.name);
    }

    private void OnMove(InputValue value)
    {
        Vector2 axisValue = value.Get<Vector2>();
        playerCharacter?.OnMovementInput(axisValue);
    }

    private void OnJump()
    {
        playerCharacter?.OnJumpInput();
    }

    private void OnTurn(InputValue value)
    {
        Vector2 axisValue = value.Get<Vector2>();
        playerCharacter?.OnTurnInput(axisValue);
    }

    private void OnZoom(InputValue value)
    {
        float axisValue = value.Get<float>();
        playerCharacter?.OnZoomInput(axisValue);
    }

    private void OnChangeControlMode()
    {
        // 현재 설정된 액션 맵 이름을 얻습니다.
        //string currentActionMapName = _PlayerInput.currentActionMap.name;

        // 다음으로 설정될 액션 맵 이름을 설정합니다.
        //string nextActionMapName = (currentActionMapName == Constants.ACTIONMAP_GAMEPLAY_MODE) ?
        //    Constants.ACTIONMAP_UI_MODE : Constants.ACTIONMAP_GAMEPLAY_MODE;
        //
        //SetControlMode(nextActionMapName);
    }

    private void OnAttackInput()
    {
        playerCharacter?.OnAttackInput();
    }

    private void OnInteraction()
    {
        playerCharacter.OnInteractionInput();
    }

    private void OnBlockPressed()
        => playerCharacter?.OnBlockInput();

    private void OnBlockReleased()
        => playerCharacter?.OnBlockInputFinished();

    private void OnRunPressed()
    => playerCharacter?.OnRunPressed();

    private void OnRunReleased()
    => playerCharacter?.OnRunReleased();

    private void OnOpenGameMenu()
    {
        // 게임 메뉴 UI 열기
        PlayerUI playerUI = SceneManagerBase.instance.GetSceneInstance<GameSceneInstance>().m_UsePlayerUI;
        playerUI.OpenGameMenuUI();

        // UI 입력 모드로 전환
        SetControlMode(Constants.ACTIONMAP_UI_MODE);
    }

    /// <summary>
    /// 캐릭터 조종을 시작합니다.
    /// </summary>
    /// <param name="playerCharacter">조종을 시작시킬 캐릭터를 전달합니다.</param>
    public void StartControl(PlayerCharacter playerCharacter)
    {
        // 같은 객체를 조종하려고 하는 경우 함수 호출 종료
        if (this.playerCharacter == playerCharacter) return;
        this.playerCharacter = playerCharacter;

        playerCharacter.OnControlStarted(this);

        // 캐릭터 데이터 갱신
        playerCharacter.OnCharacterDataUpdated(true);
    }

    /// <summary>
    /// 캐릭터 조종을 끝냅니다.
    /// </summary>
    public void FinishControl()
    {
        // 조종하는 캐릭터가 존재하지 않는다면 함수 호출 종료
        if (playerCharacter == null) return;
        playerCharacter.OnControlFinished();
        playerCharacter = null;
    }

    public void SetControlMode(string actionMap)
    {
        // 액션 맵을 설정합니다.
        _PlayerInput.SwitchCurrentActionMap(actionMap);

        // 설정된 액션 맵에 따라 행동을 정의합니다.
        switch (actionMap)
        {
            case Constants.ACTIONMAP_UI_MODE:
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
                break;

            case Constants.ACTIONMAP_GAMEPLAY_MODE:
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
                break;
        }



    }


}
