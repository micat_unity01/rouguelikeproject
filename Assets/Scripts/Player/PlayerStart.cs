using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif


public class PlayerStart : MonoBehaviour
{
    private float _Height = 2.0f;
    private float _Radius = 0.5f;

    /// <summary>
    /// 플레이어 시작 위치로 사용될 수 있는 위치를 나타냅니다.
    /// </summary>
    public Vector3 startPosition => transform.position;

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        // 오브젝트 위치
        Vector3 center = transform.position;

        // 그려질 색상을 정의합니다.
        Handles.color = Color.blue;

        // 앞 방향 화살표 그리기
        for (int i = -1; i < 2; i += 2)
        {
            Handles.ArrowHandleCap(
                0, transform.position + (Vector3.up * i * (_Height * 0.5f)),
                transform.rotation,
                1.0f,
                EventType.Repaint);
        }


        // 호 그리기
        Vector3 upCenter = center + (Vector3.up * (_Height * 0.5f - _Radius));
        Vector3 downCenter = center + (Vector3.down * (_Height * 0.5f - _Radius));
;
        Handles.DrawWireArc(upCenter, transform.forward, transform.right, 180.0f, _Radius);
        Handles.DrawWireArc(upCenter, transform.right * -1, transform.forward, 180.0f, _Radius);
        Handles.DrawWireArc(downCenter, transform.forward * -1, transform.right, 180.0f, _Radius);
        Handles.DrawWireArc(downCenter, transform.right * -1, transform.forward * -1, 180.0f, _Radius);


        // 원 그리기
        Handles.DrawWireDisc(upCenter, transform.up, _Radius);
        Handles.DrawWireDisc(downCenter, transform.up, _Radius);

        // 오브젝트 기준 선이 그려질 방향
        Vector3[] directions = 
        {
            Vector3.forward,
            Vector3.back,
            Vector3.right,
            Vector3.left
        };

        foreach(Vector3 direction in directions)
        {
            // 선 그리기 시작 위치
            Vector3 start = center + (Vector3.up * (_Height * 0.5f - _Radius)) + (direction * _Radius);

            // 선 그리기 끝 위치
            Vector3 end = center + (Vector3.down * (_Height * 0.5f - _Radius)) + (direction * _Radius);

            // 선 그리기
            Handles.DrawLine(start, end);
        }
    }
#endif

}
