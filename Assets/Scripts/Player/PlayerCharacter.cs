using startup.single;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacter : MonoBehaviour
{
    [Header("# 스프링 오브젝트")]
    public SpringComponent m_SpringObjectPrefab;

    [Header("# 체력")]
    public float m_MaxHp;

    [Header("# 스테미너")]
    public float m_MaxStamina;

    /// <summary>
    /// 이 캐릭터를 조종하고 있는 컨트롤러를 나타냅니다.
    /// </summary>
    private PlayerController _PlayerController;

    /// <summary>
    /// 캐릭터 이동을 제어하는 컴포넌트입니다.
    /// </summary>
    private PlayerCharacterMovement _MovementComponent;

    /// <summary>
    /// 애니메이션 제어 컴포넌트
    /// </summary>
    private PlayerCharacterAnimController _AnimController;


    /// <summary>
    /// 상호작용 컴포넌트
    /// </summary>
    private PlayerInteraction _InteractionComponent;

    /// <summary>
    /// 캐릭터 데이터
    /// </summary>
    private CharacterData _CharacterData;

    /// <summary>
    /// 달리기 상태를 나타냅니다.
    /// </summary>
    private bool _IsRun;

    /// <summary>
    /// 현재 체력
    /// </summary>
    private float _CurrentHp;

    /// <summary>
    /// 현재 스테미너
    /// </summary>
    private float _CurrentStamina;

    /// <summary>
    /// 이전 XZ 위치 (스테미너 계산에 사용됩니다.)
    /// </summary>
    private Vector3 _PrevPositionXZ;

    /// <summary>
    /// 공격 제어 컴포넌트
    /// </summary>
    public PlayerCharacterAttack attackComponent { get; private set; }

    /// <summary>
    /// 스프링 오브젝트를 나타냅니다.
    /// </summary>
    public SpringComponent springObject { get; private set; }

    public PlayerCharacterMovement movementComponent => _MovementComponent ??
        (_MovementComponent = GetComponent<PlayerCharacterMovement>());

    public PlayerCharacterAnimController animController => _AnimController ??
        (_AnimController = GetComponentInChildren<PlayerCharacterAnimController>());

    public PlayerInteraction interactionComponent => _InteractionComponent ??
        (_InteractionComponent = GetComponent<PlayerInteraction>());

    /// <summary>
    /// 캐릭터 데이터에 대한 읽기 전용 프로퍼티
    /// </summary>
    public ref CharacterData characterData => ref _CharacterData;

    /// <summary>
    /// Hp 수치가 변경될 때마다 발생하는 이벤트입니다.
    /// </summary>
    public event System.Action<float> onHpValueChangedEvent;

    /// <summary>
    /// Stamina 수치가 변경될 때마다 발행하는 이벤트
    /// </summary>
    public event System.Action<float> onStaminaValueChangedEvent;




    private void Awake()
    {
        _MovementComponent = GetComponent<PlayerCharacterMovement>();
        attackComponent = GetComponent<PlayerCharacterAttack>();

        // 체력 초기화
        _CurrentHp = m_MaxHp;

        // 스테미너 초기화
        _CurrentStamina = m_MaxStamina;
    }

    private void Start()
    {
        InitializeSpringObject();

        attackComponent.onBlockStarted += movementComponent.BlockMovementInput;
        attackComponent.onBlockFinished += movementComponent.AllowMovementInput;

        // 애니메이션 이벤트 바인딩
        BindAnimationEvents();

        // 공격 이벤트 바인딩
        BindAttackEvents();
    }

    private void Update()
    {
        UpdateAnimControllerParam();
    }

    private void FixedUpdate()
    {
        UpdateSpringObjectPosition();
        UpdateStaminaValue();
    }

    private void OnTriggerEnter(Collider other)
    {
        // 무기 장착, 해제 지역 입장 확인
        switch (other.tag)
        {
            case Constants.AREA_TAG_EQUIPWEAPONZONE 
                when !attackComponent.isWeaponEquipped: 

                _MovementComponent.BlockMovementInput();
                attackComponent.OnEquipWeaponZoneEnter();
                animController.StartEquipWeaponAnimation();
                break;

            case Constants.AREA_TAG_UNEQUIPWEAPONZONE
                when attackComponent.isWeaponEquipped:

                _MovementComponent.BlockMovementInput();
                attackComponent.OnUnEquipWeaponZoneEnter();
                animController.StartUnEquipWeaponAnimation();
                break;
        }
    }

    /// <summary>
    /// 애니메이션 이벤트를 바인딩합니다.
    /// </summary>
    private void BindAnimationEvents()
    {
        animController.onMovementInputAllowed += _MovementComponent.AllowMovementInput;

        animController.onNextAttackInputCheckStarted += attackComponent.StartNextAttackCheck;
        animController.onNextAttackInputCheckFinished += attackComponent.FinishNextAttackCheck;
        animController.onAttackSequenceFinished += attackComponent.OnAttackSequenceFinished;

        animController.setAttackAreaEnable += attackComponent.SetEnableAttackArea;
    }

    /// <summary>
    /// 공격 이벤트를 바인딩합니다.
    /// </summary>
    private void BindAttackEvents()
    {
        attackComponent.onAttackAnimationRequest += animController.OnAttackAnimationRequest;

        attackComponent.onAttackStarted += _MovementComponent.BlockMovementInput;
        attackComponent.onAttackStarted += LookCameraDirection;
        attackComponent.onAttackFinished+= _MovementComponent.AllowMovementInput;

    }

    /// <summary>
    /// 애니메이션 컨트롤러 파라미터 값을 갱신합니다.
    /// </summary>
    private void UpdateAnimControllerParam()
    {
        animController.moveSpeed = _MovementComponent.velocity.magnitude;
        animController.isGrounded = _MovementComponent.isGrounded;
        animController.currentCombo = attackComponent.currentComboCount;
        animController.isBlocked = attackComponent.isBlocked;
        animController.isAttackFinished = !attackComponent.isAttack;
    }

    /// <summary>
    /// 카메라의 방향을 캐릭터가 바라보도록 합니다.
    /// </summary>
    private void LookCameraDirection()
    {
        float viewYawRotation = springObject.transform.eulerAngles.y;
        transform.rotation = Quaternion.Euler(0.0f, viewYawRotation, 0.0f);
    }

    private void InitializeSpringObject()
    {
        // 씬 객체를 얻습니다.
        GameSceneInstance sceneInstance = SceneManagerBase.instance.
            GetSceneInstance<GameSceneInstance>();

        // 스프링암을 생성합니다.
        springObject = Instantiate(m_SpringObjectPrefab);

        // 스프링 타깃을 설정합니다.
        springObject.SetTargetTransform(sceneInstance.m_UseCamera.transform);
    }

    private void UpdateSpringObjectPosition()
    {
        springObject.transform.position = transform.position;
    }

    private void UpdateStaminaValue()
    {
        Vector3 currentPositionXZ = transform.position;
        currentPositionXZ.y = 0.0f;

        if (_IsRun)
        {
            float distance = Vector3.Distance(_PrevPositionXZ, currentPositionXZ);
            _CurrentStamina -= distance * 3.0f;

            // 움직이지 않았을 경우 스태미너를 채웁니다.
            if (distance < Mathf.Epsilon)
            {
                _CurrentStamina += 0.5f;

                if (_CurrentStamina > m_MaxStamina)
                    _CurrentStamina = m_MaxStamina;
            }

            if (_CurrentStamina <= 0.0f)
            {
                _CurrentStamina = 0.0f;
                _IsRun = false;
                OnRunReleased();
            }
        }
        else
        {
            _CurrentStamina += 0.5f;

            if (_CurrentStamina > m_MaxStamina) 
                _CurrentStamina = m_MaxStamina;

        }

        // 스테미너 수치 변경 이벤트 발생
        onStaminaValueChangedEvent?.Invoke(_CurrentStamina);




        _PrevPositionXZ = currentPositionXZ;
    }

    /// <summary>
    /// 플레이어 캐릭터가 피해를 입을 경우 호출되는 메서드입니다.
    /// </summary>
    /// <param name="damagedEnemy">피해를 가한 적 캐릭터가 전달됩니다.</param>
    /// <param name="damage">피해량이 전달됩니다.</param>
    public void OnDamaged(EnemyCharacter damagedEnemy, float damage)
    {
        // 이동 블록
        movementComponent.BlockMovementInput();

        // 방향
        Vector3 direction = transform.position - damagedEnemy.transform.position;
        direction.y = 0.0f;
        direction.Normalize();

        movementComponent.AddImpulse(direction, damage * 0.3f);

        // Hit Animation 재생
        animController.StartHitAnimation();

        if(attackComponent.isAttack)
        {
            attackComponent.FinishNextAttackCheck();
            attackComponent.ClearNextAttack();
            attackComponent.OnAttackSequenceFinished();
            attackComponent.SetEnableAttackArea(false);
        }


        // 체력 감소
        _CurrentHp -= damage;

        // HP 수치 변경됨 이벤트 발생
        onHpValueChangedEvent?.Invoke(_CurrentHp);
    }

    /// <summary>
    /// 캐릭터 조종이 시작되는 경우 호출됩니다.
    /// </summary>
    public void OnControlStarted(PlayerController playerController)
    {
        _PlayerController = playerController;

        // 최대 체력 설정
        _PlayerController.playerUI.m_PlayerHp.InitializePlayerHpUI(m_MaxHp);

        // 최대 스테미너 설정
        _PlayerController.playerUI.m_PlayerStamina.InitializePlayerStaminaUI(m_MaxStamina);


        // Hp 수치 변경됨 이벤트 바인딩
        onHpValueChangedEvent += _PlayerController.playerUI.m_PlayerHp.OnHpValueChanged;

        // Stamina 수치 변경됨 이벤트 바인딩
        onStaminaValueChangedEvent += _PlayerController.playerUI.m_PlayerStamina.OnStaminaValueChanged;
    }

    /// <summary>
    /// 캐릭터 조종이 끝나는 경우 호출됩니다.
    /// </summary>
    public void OnControlFinished()
    {
        // 이벤트 바인딩 취소
        onHpValueChangedEvent -= _PlayerController.playerUI.m_PlayerHp.OnHpValueChanged;

        _PlayerController = null;
    }

    /// <summary>
    /// 캐릭터 데이터를 갱신합니다.
    /// </summary>
    /// <param name="bInitialize">초기화됨 여부를 전달합니다.</param>
    public void OnCharacterDataUpdated(bool bInitialize = false)
    {
        _CharacterData = LocalDataManager.instance.characterData;

        // 체력 설정
        m_MaxHp = _CharacterData.maxHp;

        // 스테미너 설정
        m_MaxStamina = _CharacterData.maxStamina;

        // 최대 체력 설정
        _PlayerController.playerUI.m_PlayerHp.SetMaxHp(m_MaxHp);

        // 최대 스테미너 설정
        _PlayerController.playerUI.m_PlayerStamina.SetMaxStamina(m_MaxStamina);

        // 공격력 설정
        attackComponent.atk = _CharacterData.atk;

        // 최대 이동 속력 설정
        movementComponent.m_RunSpeed = _CharacterData.maxSpeed;

        if (bInitialize)
        {
            _CurrentHp = m_MaxHp;
            _CurrentStamina = m_MaxStamina;
        }
    }

    /// <summary>
    /// 이동 입력
    /// </summary>
    /// <param name="inputAxis"></param>
    public void OnMovementInput(Vector2 inputAxis) => _MovementComponent.OnMovementInput(inputAxis);

    /// <summary>
    /// 점프 입력
    /// </summary>
    public void OnJumpInput() => _MovementComponent.OnJumpInput();

    /// <summary>
    /// 회전 입력(마우스 이동)
    /// </summary>
    /// <param name="inputAxis"></param>
    public void OnTurnInput(Vector2 inputAxis) => springObject.Turn(inputAxis.x, inputAxis.y);

    /// <summary>
    /// 줌 입력(마우스 휠)
    /// </summary>
    /// <param name="axis"></param>
    public void OnZoomInput(float axis) => springObject.AddLength(axis);

    /// <summary>
    /// 공격 입력 (마우스 왼쪽 클릭)
    /// </summary>
    public void OnAttackInput() => attackComponent.RequestAttack(Constants.ATTACKNAME_DEFAULT);

    /// <summary>
    /// 상호작용 키 입력 (F 키 입력)
    /// </summary>
    public void OnInteractionInput() => interactionComponent.TryInteraction();

    /// <summary>
    /// 방어 키 입력
    /// </summary>
    public void OnBlockInput() => attackComponent.RequestBlock();

    public void OnBlockInputFinished() => attackComponent.FinishBlock();


    public void OnRunPressed()
    {
        _IsRun = true;
        _MovementComponent?.RunStart();
    }


    public void OnRunReleased()
    {
        _IsRun = false;
        _MovementComponent?.RunFinish();
    }

}

