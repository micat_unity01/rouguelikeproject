using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class BehaviorController_Enemy : BehaviorController
{
    private NavMeshAgent _NavMeshAgent;
    public NavMeshAgent navMeshAgent => _NavMeshAgent ?? (_NavMeshAgent = GetComponent<NavMeshAgent>());

    /// <summary>
    /// 생성된 위치
    /// 랜덤 이동을 사용할 때, 이동 범위를 제한하기 위하여 사용됩니다.
    /// </summary>
    public const string KEYNAME_SPAWNPOSITION = "SpawnPosition";

    /// <summary>
    /// 생성된 위치에서 이동할 수 있는 최대 거리입니다.
    /// </summary>
    public const string KEYNAME_MAXMOVEDISTANCE = "MaxMoveDistance";

    /// <summary>
    /// 랜덤한 위치로 이동하기 위한 목표 위치를 나타내는 키 입니다.
    /// </summary>
    public const string KEYNAME_MOVETARGETPOSITION = "MoveTargetPosition";

    /// <summary>
    /// 공격적인 상태를 나타내는 키
    /// </summary>
    public const string KEYNAME_ISAGGRESSIVESTATE = "IsAggressiveState";

    /// <summary>
    /// 플레이어 캐릭터를 잠시 저장하기 위한 키
    /// </summary>
    public const string KEYNAME_PLAYERCHARACTER = "PlayerCharacter";

    /// <summary>
    /// 공격이 요청되었음을 나타내기 위한 키
    /// </summary>
    public const string KEYNAME_ATTACKREQUESTED = "AttackRequested";

    protected virtual void Awake()
    {
        AddKey(KEYNAME_SPAWNPOSITION, transform.position);  // 생성 위치 키 추가
        AddKey(KEYNAME_MAXMOVEDISTANCE);                    // 최대 이동 반경 키 추가
        AddKey(KEYNAME_MOVETARGETPOSITION);                 // 이동 목표 위치 키 추가
        AddKey(KEYNAME_ISAGGRESSIVESTATE, false);           // 공격적 상태를 나타내는 키 추가
        AddKey(KEYNAME_PLAYERCHARACTER);                    // 플레이어 캐릭터를 저장하기 위한 키 추가
        AddKey(KEYNAME_ATTACKREQUESTED, false);             // 공격 요청됨 상태 키 추가
        
    }
    protected virtual void Start()
    {

    }


}
