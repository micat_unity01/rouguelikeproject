
using System;
using System.Collections;
/// <summary>
/// 등록된 행동들을 순차적으로 실행하는 객체입니다.
/// 등록된 순서대로 행동을 실행하며, 실행한 행동이 성공한다면 분기를 종료시킵니다.
/// </summary>
public class BehaviorSelector : BehaviorCompositeBase
{
    public BehaviorSelector() { }
    public BehaviorSelector(params Func<RunnableBehavior>[] runnables) : base(runnables) { }

    public override IEnumerator RunBehavior()
    {
        isSucceeded = false;

        foreach(System.Func<RunnableBehavior> getRunnable in m_Runnable)
        {
            // 등록된 행동 객체를 생성합니다.
            RunnableBehavior runnable = getRunnable();

            // 초기화
            runnable.OnRunnableInitialized(behaviorController);

            // 행동이 변경됨
            behaviorController.OnBehaviorChanged(runnable);

            // 행동을 실행합니다.
            yield return runnable.RunBehavior();

            // 행동 끝남
            runnable.OnBehaviorFinished();

            // 실행한 행동이 성공한 경우 다음 행동을 실행하지 않도록 합니다.
            if (runnable.isSucceeded)
            {
                // 다음 행동을 실행하지 않도록 합니다.
                isSucceeded = true;
                yield break;
            }
        }
    }
}