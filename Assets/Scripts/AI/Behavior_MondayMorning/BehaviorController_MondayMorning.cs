using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BehaviorController_MondayMorning : BehaviorController_Enemy
{
    /// <summary>
    /// 공격중임을 알리기 위한 키
    /// </summary>
    public const string KEYNAME_ISATTACKING = "IsAttacking";

    protected override void Awake()
    {
        base.Awake();

        AddKey(KEYNAME_ISATTACKING, false);                 // 공격중임을 나타내기 위한 키 추가

        SetKey(KEYNAME_MAXMOVEDISTANCE, 10.0f);             // 최대 이동 반경 설정
    }

    protected override void Start()
    {
        base.Start();

        StartBehavior();
    }

    public void StartBehavior()
    {
        StartBehavior<RootSelector_MondayMorning>();
    }

}
