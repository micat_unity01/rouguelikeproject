using UnityEngine;

public class RootSequencer_Dummy : BehaviorSequencer
{ 
    public RootSequencer_Dummy()
    {
        // 랜덤 위치 설정 행동 추가
        AddBehavior<BT_GetRadomPositionInNavigableRadius>();

        // 지정한 위치로 이동하는 행동 추가
        AddBehavior(() => new BT_MoveTo(BehaviorController_Enemy.KEYNAME_MOVETARGETPOSITION));
    }

}
