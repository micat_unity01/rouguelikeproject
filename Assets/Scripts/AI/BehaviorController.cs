using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 등록된 행동들을 관리할 객체
/// </summary>
public class BehaviorController : MonoBehaviour
{

    /// <summary>
    /// 행동 객체에서 사용하게 될 데이터들을 가집니다.
    /// 외부에서 행동 객체에게 직접 접근하는 것이 어려우므로
    /// 사용되는 모든 데이터들은 BehaviorController 에서 소유하고 있도록 설계되었습니다.
    /// </summary>
    protected Dictionary<string, object> m_Keys = new();

    /// <summary>
    /// 현재 행동을 나타냅니다.
    /// </summary>
    private RunnableBehavior _CurrentBehavior;

    /// <summary>
    /// 행동 루틴을 나타내는 객체입니다.
    /// 오브젝트 파괴시 종료시키기 위하여 사용됩니다.
    /// </summary>
    private Coroutine _BehaviorRoutine;


    public void StartBehavior<TRootRunnalbe>()
        where TRootRunnalbe : RunnableBehavior, new()
    {
        Debug.Log("StartBehavior");
        _BehaviorRoutine = StartCoroutine(Run<TRootRunnalbe>());
    }

    /// <summary>
    /// 행동 루틴을 실행시킵니다.
    /// </summary>
    /// <returns></returns>
    private IEnumerator Run<TRootRunnalbe>()
        where TRootRunnalbe : RunnableBehavior, new()
    {

        while (true)
        {
            // 행동 객체 생성
            TRootRunnalbe rootRunnable = new TRootRunnalbe();

            // 초기화
            rootRunnable.OnRunnableInitialized(this);

            // 행동 변경됨
            OnBehaviorChanged(rootRunnable);

            // 행동 실행
            yield return rootRunnable.RunBehavior();

            // 행동 끝
            rootRunnable?.OnBehaviorFinished();
        }
    }

    /// <summary>
    /// 행동이 교체될 때 호출됩니다.
    /// </summary>
    /// <param name="newBeavior">교체되는 새로운 행동 객체가 전달됩니다.</param>
    public virtual void OnBehaviorChanged(RunnableBehavior newBeavior)
    {
        _CurrentBehavior = newBeavior;
    }

    /// <summary>
    /// 행동 루틴을 종료합니다.
    /// </summary>
    public void StopBehavior()
    {
        // 행동 끝
        if (_CurrentBehavior != null)
        {
            _CurrentBehavior?.OnBehaviorFinished();
            _CurrentBehavior = null;
        }

        // 실행중인 루틴을 종료시킵니다.
        if (_BehaviorRoutine != null)
        {
            StopCoroutine(_BehaviorRoutine);
            _BehaviorRoutine = null;
        }
    }

    /// <summary>
    /// 사용될 키를 추가합니다.
    /// </summary>
    /// <param name="keyName">키 이름을 전달합니다.</param>
    /// <param name="initialValue">키의 기본 값을 설정합니다.</param>
    public void AddKey(string keyName, object initialValue = null)
        => m_Keys.Add(keyName, initialValue);

    /// <summary>
    /// 키의 값을 설정합니다.
    /// </summary>
    /// <param name="keyName">키 이름을 전달합니다.</param>
    /// <param name="value">설정시킬 값을 전달합니다.</param>
    public void SetKey(string keyName, object value)
        => m_Keys[keyName] = value;

    /// <summary>
    /// 키의 값을 얻습니다.
    /// </summary>
    /// <param name="keyName">키 이름을 전달합니다.</param>
    /// <returns></returns>
    public object GetKey(string keyName)
    {
        if (m_Keys.TryGetValue(keyName, out object value)) return value;
        return null;
    }

    public T GetKeyAsValue<T>(string keyName) where T : struct
        => (T)GetKey(keyName);

    public T GetKeyAsObject<T>(string keyName) where T : class 
        => GetKey(keyName) as T;

    protected virtual void OnDestroy()
    {
        // 행동 루틴을 종료합니다.
        StopBehavior();
    }

#if UNITY_EDITOR
    protected virtual void OnDrawGizmos()
    {
        if (!UnityEditor.EditorApplication.isPlaying) return;

        // 현재 실행중인 Task/Composite 의 OnDrawGizmos() 호출
        _CurrentBehavior.OnDrawGizmos();

        // 현재 실행중인 Task/Composite 에 추가된 Service 의 OnDrawGizmos() 호출
        if (_CurrentBehavior.behaviorServices != null)
        {
            foreach(BehaviorService service in _CurrentBehavior.behaviorServices)
                service.OnDrawGizmos();
        }
    }

    protected virtual void OnDrawGizmosSelected()
    {
        if (!UnityEditor.EditorApplication.isPlaying) return;

        // 현재 실행중인 Task/Composite 의 OnDrawGizmosSelected() 호출
        _CurrentBehavior.OnDrawGizmosSelected();

        // 현재 실행중인 Task/Composite 에 추가된 Service 의 OnDrawGizmosSelected() 호출
        if (_CurrentBehavior.behaviorServices != null)
        {
            foreach (BehaviorService service in _CurrentBehavior.behaviorServices)
                service.OnDrawGizmosSelected();
        }

        // 추가된 키와, 실행중인 행동 대한 값을 표시합니다.
        string contextString = $"Running [{_CurrentBehavior.GetType().Name}]\n\n";
        foreach(KeyValuePair<string, object> elem in m_Keys)
        {
            string keyName = elem.Key;
            object value = elem.Value;
            contextString += $"{keyName}  [{value}]\n";
        }

        GUIStyle style = new();
        style.normal.textColor = Color.black;
        style.fontSize = 12;
        style.alignment = TextAnchor.LowerLeft;
        UnityEditor.Handles.Label(transform.position, contextString, style);
    }
#endif





}
