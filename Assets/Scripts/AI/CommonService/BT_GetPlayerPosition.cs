using System.Collections;
using UnityEngine;

public class BT_GetPlayerPosition : RunnableBehavior
{
    /// <summary>
    /// 목표 위치 키
    /// </summary>
    private string _TargetPositionKey;

    /// <summary>
    /// 플레이어 캐릭터 객체를 나타내는 키
    /// </summary>
    private string _PlayerCharacterKey;

    /// <summary>
    /// 최대 추적 거리입니다.
    /// </summary>
    private float _MaxTrackingDistance;

    public BT_GetPlayerPosition(
        string targetPositionKey, 
        string palyerCharacterKey,
        float maxTrackingDistance)
    {
        _TargetPositionKey = targetPositionKey;
        _PlayerCharacterKey = palyerCharacterKey;
        _MaxTrackingDistance = maxTrackingDistance;
    }

    public override IEnumerator RunBehavior()
    {
        // 플레이어 캐릭터 객체를 얻습니다.
        PlayerCharacter playerCharacter = behaviorController.
            GetKeyAsObject<PlayerCharacter>(_PlayerCharacterKey);

        // 플레이어 캐릭터를 가져오지 못한 경우 행동 실패
        if (playerCharacter == null)
        {
            isSucceeded = false;
            yield break;
        }

        // 목표 위치를 얻습니다.
        Vector3 targetPosition = GetPlayerPosition(playerCharacter);

        // 현재 위치
        Vector3 currentPosition = behaviorController.transform.position;

        // 목표 위치까지의 거리를 구합니다.
        float distanceToTarget = Vector3.Distance(targetPosition, currentPosition);

        // 목표까지의 거리가 최대 추적 거리보다 먼 경우
        if (_MaxTrackingDistance < distanceToTarget)
        {
            // 목표까지의 방향
            Vector3 direction = (targetPosition - currentPosition).normalized;

            // 목표 위치를 재설정합니다.
            targetPosition = currentPosition + direction * _MaxTrackingDistance;
        }

        // 목표 위치를 설정합니다.
        behaviorController.SetKey(_TargetPositionKey, targetPosition);

        isSucceeded = true;
    }

    /// <summary>
    /// 플레이어 캐릭터 위치를 얻습니다.
    /// </summary>
    /// <param name="playerCharacter">플레이어 캐릭터 객체를 전달합니다.</param>
    /// <returns></returns>
    private Vector3 GetPlayerPosition(PlayerCharacter playerCharacter)
    {
        return playerCharacter.transform.position;
    }
}
