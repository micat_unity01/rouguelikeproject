using startup.single;
using System.Collections;
using UnityEngine;

public class BS_DoDetectPlayer : BehaviorService
{
    /// <summary>
    /// 감지 반경
    /// </summary>
    private float _DetectRadius;

    /// <summary>
    /// 플레이어 캐릭터 레이어
    /// </summary>
    private int _PlayerCharacterLayer;

    #region DEBUG
    private DrawGizmoSphereInfo _DrawAttackAreaInfo;
    #endregion

    public BS_DoDetectPlayer(float detectRadius, int playerCharacterLayer)
    {
        _DetectRadius = detectRadius;
        _PlayerCharacterLayer = playerCharacterLayer;
    }


    public override void ServiceTick()
    {
        CheckArea();
    }

    private void CheckArea()
    {
        Collider[] detectCollisions = PhysicsExt.OverlapSphere(out _DrawAttackAreaInfo,
            behaviorController.transform.position,
            _DetectRadius,
            _PlayerCharacterLayer);

        // 플레이어 캐릭터를 감지한 경우
        if (detectCollisions.Length > 0)
        {
            // Get SceneInstance
            GameSceneInstance sceneInstance = 
                SceneManagerBase.instance.GetSceneInstance<GameSceneInstance>();

            // 플레이어 캐릭터를 설정합니다.
            behaviorController.SetKey(
                BehaviorController_Enemy.KEYNAME_PLAYERCHARACTER, 
                sceneInstance.playerController.playerCharacter);

            // 목표 위치를 현재 위치로 설정하여 이동을 중단시킵니다.
            behaviorController.SetKey(
                BehaviorController_Enemy.KEYNAME_MOVETARGETPOSITION,
                behaviorController.transform.position);

            // 공격 요청
            behaviorController.SetKey(
                BehaviorController_Enemy.KEYNAME_ATTACKREQUESTED, true);
        }
    }

    public override void OnDrawGizmos()
    {
        base.OnDrawGizmos();


    }
}

