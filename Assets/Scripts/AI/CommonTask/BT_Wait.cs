using System.Collections;
using UnityEngine;

public class BT_Wait : RunnableBehavior
{
    private WaitForSeconds _WaitSeconds;

    public BT_Wait(float waitSeconds)
    {
        _WaitSeconds = new(waitSeconds);
    }

    public override IEnumerator RunBehavior()
    {
        yield return _WaitSeconds;
        isSucceeded = true;
    }
}