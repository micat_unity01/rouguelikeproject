
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "AttackData", menuName = "ScriptableObject/AttackData")]
public class AttackDataScriptableObject : ScriptableObject
{
    /// <summary>
    /// 공격 데이터들
    /// </summary>
    public List<AttackDataElem> m_AttackDatas;

    public AttackDataElem GetAttackDataFromName(string attackName)
    {
        return m_AttackDatas.Find((AttackDataElem elem) => elem.m_AttackName == attackName);
    }
}