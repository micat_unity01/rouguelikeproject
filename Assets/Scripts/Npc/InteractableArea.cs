using startup.single;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SphereCollider))]
public class InteractableArea : MonoBehaviour
{
    /// <summary>
    /// 이 컴포넌트를 갖는 NPC 객체를 나타냅니다.
    /// </summary>
    private NpcBase _Npc;

    public SphereCollider interactableArea { get; private set; }

    private void Awake()
    {
        _Npc = GetComponent<NpcBase>();
        interactableArea = GetComponent<SphereCollider>();
        interactableArea.isTrigger = true;
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag != Constants.TAG_PLAYER) return;

        // 상호작용 가능하도록 합니다.
        GameSceneInstance sceneInstance = SceneManagerBase.instance.GetSceneInstance<GameSceneInstance>();
        PlayerInteraction interactionComponent = null;

        // NullReferenceException 예외처리
        try
        {
            interactionComponent = sceneInstance.playerController.playerCharacter.interactionComponent;
        }
        catch (NullReferenceException) { return; }

        interactionComponent.AddInteractableNpc(_Npc);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag != Constants.TAG_PLAYER) return;

        // 상호작용 가능하도록 합니다.
        GameSceneInstance sceneInstance = SceneManagerBase.instance.GetSceneInstance<GameSceneInstance>();
        PlayerInteraction interactionComponent = null;

        // NullReferenceException 예외처리
        try
        {
            interactionComponent = sceneInstance.playerController.playerCharacter.interactionComponent;
        }
        catch (NullReferenceException) { return; }

        interactionComponent.RemoveInteractableNpc(_Npc);
    }



}
