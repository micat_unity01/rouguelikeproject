using startup.single;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class EnemyCharacter : MonoBehaviour
{
    /// <summary>
    /// 적 정보를 담고 있는 Scriptable Object 에셋입니다.
    /// </summary>
    protected static EnemyDataScriptableObject m_EnemyDataScriptableObject;

    [Header("# HUD")]
    public EnemyHUD m_EnemyHUDPrefab;

    [Header("# 적 코드")]
    public string m_EnemyCode;

    /// <summary>
    /// HUD 가 표시될 위치를 나타냅니다.
    /// </summary>
    public Transform m_EnemyHUDWorldPosition;

    /// <summary>
    /// Navigation System 을 사용하여 길찾기를 수행할 컴포넌트를 나타냅니다.
    /// </summary>
    private NavMeshAgent _Agent;

    /// <summary>
    /// 적 캐릭터 영역을 나타내는 Collider Component 입니다.
    /// </summary>
    private Collider _EnemyCollider;

    /// <summary>
    /// 애니메이션 제어 컴포넌트입니다.
    /// </summary>
    private EnemyAnimController _AnimController;

    /// <summary>
    /// 행동 제어 컴포넌트를 나타냅니다.
    /// </summary>
    private BehaviorController_Enemy _BehaviorController;

    /// <summary>
    /// 적 정보를 나타냅니다.
    /// </summary>
    protected EnemyData m_EnemyData;

    /// <summary>
    /// 현재 체력을 나타냅니다.
    /// </summary>
    protected float m_Hp;

    /// <summary>
    /// _Agent 에 대한 읽기 전용 프로퍼티입니다.
    /// </summary>
    /// 
    public NavMeshAgent agent => _Agent ?? (_Agent = GetComponent<NavMeshAgent>());

    /// <summary>
    /// 적 캐릭터 영역에 대한 읽기 전용 프로퍼티입니다.
    /// </summary>
    public Collider enemyCollider => _EnemyCollider ?? (_EnemyCollider = GetComponent<Collider>());

    /// <summary>
    /// 애니메이션 제어 컴포넌트에 대한 읽기 전용 프로퍼티입니다.
    /// </summary>
    public EnemyAnimController animController => _AnimController ?? (_AnimController = GetComponentInChildren<EnemyAnimController>());

    /// <summary>
    /// 행동 제어 컴포넌트에 대한 읽기 전용 프로퍼티입니다.
    /// </summary>
    public BehaviorController_Enemy behaviorController => _BehaviorController ??
        (_BehaviorController = GetComponent<BehaviorController_Enemy>());

    /// <summary>
    /// 사용되는 HUD 객체를 나타냅니다.
    /// </summary>
    public EnemyHUD enemyHUD { get; private set; }

    /// <summary>
    /// 적 정보에 대한 읽기 전용 프로퍼티입니다.
    /// </summary>
    public ref EnemyData enemyData => ref m_EnemyData;

    protected virtual void Awake()
    {
        if (m_EnemyDataScriptableObject == null)
        {
            m_EnemyDataScriptableObject = Resources.Load<EnemyDataScriptableObject>(
                "ScriptableObject/EnemyData");
        }

        // 적 캐릭터 레이어 설정
        gameObject.layer = LayerMask.NameToLayer(Constants.LAYERNAME_ENEMY);

        // 씬 객체에 적 객체를 등록합니다.
        GameSceneInstance sceneInst = SceneManagerBase.instance.GetSceneInstance<GameSceneInstance>();
        sceneInst.RegisterEnemyCharacter(this);

        // 적 정보 가져오기
        m_EnemyData = m_EnemyDataScriptableObject.GetEnemyData(m_EnemyCode);
        m_Hp = m_EnemyData.maxHp;

        // HUD 객체 생성
        enemyHUD = sceneInst.m_UsePlayerUI.CreateHUD(m_EnemyHUDPrefab);
        enemyHUD.InitializeEnemyHUD(m_EnemyData.enemyName, m_EnemyData.maxHp);
    }

    protected virtual void Update()
    {
        UpdateNavMeshAgentStopState();

        UpdateHUDPosition();
    }

    /// <summary>
    /// HUD 위치를 갱신합니다.
    /// </summary>
    private void UpdateHUDPosition()
    {
        enemyHUD.SetWorldPosition(m_EnemyHUDWorldPosition.position);
    }

    /// <summary>
    /// NavMeshAgent 의 멈춤 상태를 갱신합니다.
    /// </summary>
    private void UpdateNavMeshAgentStopState()
    {
        Vector3 bottomPosition = transform.position + Vector3.down * agent.baseOffset;

        // 목적지에 도달했는지확인합니다.
        bool isGoal = (agent.remainingDistance <= agent.stoppingDistance) &&
            (Vector3.Distance(agent.destination, bottomPosition) <= agent.stoppingDistance);

        // 속력이 0으로 설정되어있는지 확인합니다.
        bool isStopped = agent.velocity.sqrMagnitude == 0.0f;

        // 멈춤 상태를 설정합니다.
        agent.isStopped = (isGoal && isStopped);
    }

    public virtual void OnDamaged(PlayerCharacter playerCharacter, float damage)
    {
        // 피해를 입히기 전, 수치들을 연산합니다.
        damage = CalculateDamage(damage);

        m_Hp -= damage;

        // 피해를 입힌 객체 (플레이어 캐릭터) 를 잠시 설정합니다.
        behaviorController.SetKey(
            BehaviorController_Enemy.KEYNAME_PLAYERCHARACTER, playerCharacter);

        // 공격적인 상태로 변경합니다.
        behaviorController.SetKey(BehaviorController_Enemy.KEYNAME_ISAGGRESSIVESTATE, true);

        // 공격 결과 HUD 표시
        ShowAttackResult(transform.position);

        // 사망 이벤트
        if (m_Hp < 0.0f)
        {
            OnDead();
            m_Hp = 0.0f;
        }

        enemyHUD.SetHp(m_Hp);
    }

    /// <summary>
    /// 적이 사망한 경우 호출되는 메서드입니다.
    /// </summary>
    public virtual void OnDead()
    {
        if (enemyHUD)
        {
            // HUD 제거
            Destroy(enemyHUD.gameObject);
        }
    }

    /// <summary>
    /// 대미지를 계산합니다.
    /// </summary>
    /// <param name="damage">입은 피해량을 전달합니다.</param>
    /// <returns>수치들을 연산시킨 결과를 반환합니다.</returns>
    protected virtual float CalculateDamage(float damage)
    {
        return damage;
    }

    /// <summary>
    /// 공격 결과 HUD 를 표시합니다.
    /// </summary>
    /// <param name="hitPosition">공격을 당한 위치를 전달합니다.</param>
    protected virtual void ShowAttackResult(Vector3 hitPosition)
    {
        GameSceneInstance sceneInstance =
            SceneManagerBase.instance.GetSceneInstance<GameSceneInstance>();
        sceneInstance.attackResultHUDController.ShowResult(hitPosition);
    }



}
