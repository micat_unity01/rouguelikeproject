using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class MondayMorning : EnemyCharacter
{

    /// <summary>
    /// 애니메이션 파라미터 _MoveSpeed 에 적용시킬 수치
    /// </summary>
    private float _AnimParamMoveSpeedValue;

    /// <summary>
    /// 공격 기능을 제공하기 위한 컴포넌트
    /// </summary>
    private MondayMorningAttack _AttackComponent;

    /// <summary>
    /// 랙돌을 위한 Rigidbody Component 들
    /// </summary>
    private List<Rigidbody> m_SpineRigidBodies;

    /// <summary>
    /// 공격 컴포넌트에 대한 읽기 전용 프로퍼티
    /// </summary>
    public MondayMorningAttack attackComponent => _AttackComponent ??
        (_AttackComponent = GetComponent<MondayMorningAttack>());

    protected override void Awake()
    {
        base.Awake();

        m_SpineRigidBodies = new(GetComponentsInChildren<Rigidbody>());
        foreach(Rigidbody rigidbody in m_SpineRigidBodies)
        {
            rigidbody.useGravity = false;
            rigidbody.isKinematic = true;
            rigidbody.gameObject.layer = LayerMask.NameToLayer(Constants.LAYERNAME_RAGDOLL);
        }
    }

    protected override void Update()
    {
        base.Update();

        UpdateAnimParam();
    }


    private void UpdateAnimParam()
    {
        _AnimParamMoveSpeedValue = Mathf.MoveTowards(
            _AnimParamMoveSpeedValue, agent.velocity.magnitude, 10.0f * Time.deltaTime);

        // 속력값 갱신
        animController.SetFloat(Constants.ANIMPARAM_MONDAYMORNING_MOVESPEED,
            _AnimParamMoveSpeedValue);
    }

    public override void OnDamaged(PlayerCharacter playerCharacter, float damage)
    {
        base.OnDamaged(playerCharacter, damage);

        MondayMorningAnimController animController = (this.animController as MondayMorningAnimController);

        // 공격 영역 비활성화
        attackComponent.DisableAttackArea();

        // 공격 끝내기
        animController.AnimEvent_AttackFinished();

        // Hit 애니메이션 재생
        animController.PlayHitAnimation();

        // 피해를 입을 경우 행동을 재실행합니다.
        behaviorController.StopBehavior();
        (behaviorController as BehaviorController_MondayMorning).StartBehavior();
    }

    public override void OnDead()
    {
        base.OnDead();


        // 애니메이터 중단
        animController.animator.enabled = false;

        // 행동 중지
        behaviorController.StopBehavior();

        foreach (Rigidbody rigidbody in m_SpineRigidBodies)
        {

            rigidbody.useGravity = true;
            rigidbody.isKinematic = false;
            rigidbody.velocity = Vector3.zero;
        }

        // 5 초 뒤 오브젝트가 제거되도록 합니다.
        Destroy(gameObject, 5.0f);
    }
}
