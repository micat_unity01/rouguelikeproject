using UnityEngine;

/// <summary>
/// 월요일 아침 적 캐릭터의 애니메이션을 제어하기 위한 컴포넌트입니다.
/// </summary>
public class MondayMorningAnimController : EnemyAnimController
{ 
    public void PlayAttackAnimation()
    {
        SetTrigger("_Attack");
    }

    public void PlayAttackBlockedAnimation()
    {
        SetTrigger("_IsAttackBlocked");
    }

    public void PlayHitAnimation()
    {
        SetTrigger("_IsDamaged");
    }

    /// <summary>
    /// 공격이 끝났을 경우를 나타내는 애니메이션 이벤트
    /// </summary>
    public void AnimEvent_AttackFinished()
    {
        // 공격 상태 종료
        ownerEnemyCharacter.behaviorController.SetKey(
            BehaviorController_MondayMorning.KEYNAME_ISATTACKING, false);
    }

    /// <summary>
    /// 공격 영역 활성화 애니메이션 이벤트
    /// </summary>
    public void AnimEvent_EnableAttackArea()
        => (ownerEnemyCharacter as MondayMorning).attackComponent.EnableAttackArea();

    /// <summary>
    /// 공격 영역 비활성화 애니메이션 이벤트
    /// </summary>
    public void AnimEvent_DisableAttackArea()
        => (ownerEnemyCharacter as MondayMorning).attackComponent.DisableAttackArea();

}
