using startup.single;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MondayMorningAttack : MonoBehaviour
{
    [Header("# 소켓")]
    public Transform m_WeaponAttackAreaStart;
    public Transform m_WeaponAttackAreaEnd;

    private MondayMorning _EnemyCharacter;

    /// <summary>
    /// 공격 시 감지한 객체를 잠시 저장해둘 리스트
    /// </summary>
    private List<Collider> _AttackAreaDetected = new();

    /// <summary>
    /// 공격 영역 활성화 상태를 나타냅니다.
    /// </summary>
    private bool _AttackAreaEnabled;

    #region DEBUG
    private DrawGizmoSphereInfo _AttackAreaDrawGizmoInfo;
    #endregion

    private void Awake()
    {
        _EnemyCharacter = GetComponent<MondayMorning>();
    }

    private void FixedUpdate()
    {
        if (_AttackAreaEnabled)
        {
            // 공격 영역을 확인하고, 플레이어 캐릭터가 감지되면 피해를 입힙니다.
            if (CheckAttackArea(out RaycastHit hitResult))
            {
                if (!_AttackAreaDetected.Contains(hitResult.collider))
                {
                    _AttackAreaDetected.Add(hitResult.collider);

                    // 감지된 객체에게 피해를 입힙니다.
                    ApplyDamage(hitResult);
                }
            }
        }
    }

    /// <summary>
    /// 플레이어 캐릭터 방향으로 회전합니다.
    /// </summary>
    private void LookAtPlayerCharacter()
    {
        // 플레이어 캐릭터 객체를 얻습니다.
        PlayerCharacter playerCharacter = _EnemyCharacter.behaviorController.GetKeyAsObject<PlayerCharacter>(
            BehaviorController_Enemy.KEYNAME_PLAYERCHARACTER);

        // 플레이어 캐릭터 객체 유효성 검사
        if (playerCharacter == null) return;

        // 플레이어 캐릭터로 향하는 방향을 얻습니다.
        Vector3 directionToPlayerCharacter = playerCharacter.transform.position - transform.position;
        directionToPlayerCharacter.y = 0.0f;
        directionToPlayerCharacter.Normalize();

        // 플레이어 캐릭터 방향으로 회전시킬 각도를 구합니다.
        float lookAtRotationYaw = Mathf.Atan2(
            directionToPlayerCharacter.x, directionToPlayerCharacter.z) * Mathf.Rad2Deg;

        // 회전각 설정
        transform.rotation = Quaternion.Euler(0.0f, lookAtRotationYaw, 0.0f);

        Debug.Log("lookAtRotationYaw = " + lookAtRotationYaw);
    }

    private bool CheckAttackArea(out RaycastHit hitInfo)
    {
        Vector3 start = m_WeaponAttackAreaStart.position;
        Vector3 end = m_WeaponAttackAreaEnd.position;
        Vector3 direction = (end - start).normalized;
        float distance = Vector3.Distance(start, end);

        Ray ray = new Ray(start, direction);

        bool isHit = PhysicsExt.SphereCast(
            out _AttackAreaDrawGizmoInfo,
            ray,
            0.3f,
            out hitInfo,
            distance,
            LayerMask.GetMask(Constants.LAYERNAME_PLAYER));

        return isHit;
    }

    /// <summary>
    /// 플레이어에게 피해를 가합니다.
    /// </summary>
    /// <param name="hitResult">감지 결과를 전달합니다.</param>
    private void ApplyDamage(RaycastHit hitResult)
    {
        // Get Player Character
        GameSceneInstance sceneInstasnce = SceneManagerBase.instance.GetSceneInstance<GameSceneInstance>();
        PlayerCharacter playerCharacter = sceneInstasnce.playerController.playerCharacter;
        //PlayerCharacter playerCharacter = hitResult.transform.GetComponent<PlayerCharacter>()

        // 플레이어 캐릭터가 null인 경우 함수 호출 종료.
        if (playerCharacter == null) return;

        // 플레이어 캐릭터가 방어중이라면
        if (playerCharacter.attackComponent.isBlocked)
        {
            // 공격 영역 비활성화
            DisableAttackArea();

            // 공격 애니메이션 재생
            MondayMorningAnimController animController = (_EnemyCharacter.animController as MondayMorningAnimController);

            animController.AnimEvent_AttackFinished();
            animController.PlayAttackBlockedAnimation();
        }
        // 방어중이 아니라면
        else
        {
            // 가할 피해량을 얻습니다.
            float damage = _EnemyCharacter.enemyData.atk;

            // 플레이어 캐릭터에게 피해를 가합니다.
            playerCharacter.OnDamaged(_EnemyCharacter, damage);
        }


    }

    public void Attack()
    {
        // 플레이어 방향을 바라보도록 합니다.
        LookAtPlayerCharacter();

        // 공격 애니메이션 재생
        (_EnemyCharacter.animController as MondayMorningAnimController).PlayAttackAnimation();
    }

    /// <summary>
    /// 공격 영역 활성화
    /// </summary>
    public void EnableAttackArea()
    {
        _AttackAreaEnabled = true;
    }

    /// <summary>
    /// 공격 영역 비활성화
    /// </summary>
    public void DisableAttackArea()
    {
        _AttackAreaEnabled = false;
        _AttackAreaDetected.Clear();
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if (_AttackAreaEnabled)
        {
            PhysicsExt.DrawGizmoSphere(in _AttackAreaDrawGizmoInfo);
        }
    }
#endif

}
