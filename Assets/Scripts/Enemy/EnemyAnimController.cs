using UnityEngine;

public class EnemyAnimController : MonoBehaviour
{
    /// <summary>
    /// 이 애님 컨트롤러를 소유하는 적 캐릭터 객체를 나타냅니다.
    /// </summary>
    private EnemyCharacter _OwnerEnemyCharacter;

    /// <summary>
    /// 제어할 애니메이터 컴포넌트를 나타냅니다.
    /// </summary>
    private Animator _Animator;

    public Animator animator => _Animator ?? (_Animator = GetComponentInChildren<Animator>());

    public EnemyCharacter ownerEnemyCharacter => _OwnerEnemyCharacter ??
        (_OwnerEnemyCharacter = GetComponentInParent<EnemyCharacter>());

    public void SetTrigger(string paramName) 
        => animator.SetTrigger(paramName);
    public void SetInt(string paramName, int value) 
        => animator.SetInteger(paramName, value);
    public void SetFloat(string paramName, float value)
        => animator.SetFloat(paramName, value);
    public void SetBool(string paramName, bool value)
        => animator.SetBool(paramName, value);


}
