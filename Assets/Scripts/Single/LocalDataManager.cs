using Newtonsoft.Json;
using startup.single;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Unity.VisualScripting;
using UnityEngine;

public sealed class LocalDataManager : ManagerClassBase<LocalDataManager>
{
    /// <summary>
    /// 설정 데이터
    /// </summary>
    public SettingData settingData { get; set; } = new();

    /// <summary>
    /// 캐릭터 데이터
    /// </summary>
    public CharacterData characterData { get; set; } = new();


    public override void OnManagerInitialized()
    {
        base.OnManagerInitialized();

        // 로컬 데이터 디렉터리 생성
        GenerateLocalDataDirectory();

        // 설정 데이터를 읽습니다. (없는 경우 생성)
        ReadSettingData();

        // 플레이어 캐릭터 데이터를 읽습니다.
        ReadCharacterData();
    }

    /// <summary>
    /// 로컬 데이터 디렉터리를 생성합니다.
    /// </summary>
    private void GenerateLocalDataDirectory()
    {
        // 경로 존재하지 않는 경우
        if (!Directory.Exists(Constants.LOCALDATA_PATH))
        {
            // 디렉터리 생성
            Directory.CreateDirectory(Constants.LOCALDATA_PATH);
        }
    }

    /// <summary>
    /// 설정 데이터를 읽습니다.
    /// </summary>
    private void ReadSettingData()
    {
        // 설정 파일 경로
        string settingDataFilePath = Constants.LOCALDATA_PATH + Constants.SETTINGDATA_FILENAME;

        // 설정 파일이 존재하는 경우
        if (File.Exists(settingDataFilePath))
        {
            string readText = File.ReadAllText(settingDataFilePath);
            settingData = JsonConvert.DeserializeObject<SettingData>(readText);
        }
        // 설정 파일이 존재하지 않는 경우, 새로운 파일을 생성합니다.
        else
        {
            // 데이터 생성
            SettingData newSettingData = new SettingData();

            newSettingData.width = Screen.width;
            newSettingData.height = Screen.height;
            newSettingData.screenMode = Screen.fullScreenMode;

            settingData = newSettingData;

            // Setting Data 를 Json 문자열로 변환합니다.
            string jsonString = JsonConvert.SerializeObject(settingData);

            // 파일을 생성합니다.
            File.WriteAllText(settingDataFilePath, jsonString);
        }
    }

    /// <summary>
    /// 플레이어 캐릭터 데이터를 읽습니다.
    /// </summary>
    private void ReadCharacterData()
    {
        // 캐릭터 데이터 경로
        string characterDataPath = Constants.LOCALDATA_PATH + Constants.CHARACTERDATA_FILENAME;

        // 캐릭터 데이터 파일이 존재하는 경우
        if (File.Exists(characterDataPath))
        {
            // 읽기
            string characterDataJson = File.ReadAllText(characterDataPath);
            characterData = JsonConvert.DeserializeObject<CharacterData>(characterDataJson);
        }
        // 없는 경우
        else
        {
            // 캐릭터 기본 수치로 초기화합니다.
            characterData = CharacterData.Initialize();


            // 캐릭터 데이터를 JSON 문자열로 변환합니다.
            string characterDataToJson = JsonConvert.SerializeObject(characterData);

            // 파일을 생성합니다.
            File.WriteAllText(characterDataPath, characterDataToJson);
        }
    }

    /// <summary>
    /// 설정 데이터 쓰기
    /// </summary>
    public void WriteSettingData()
    {
        // 현재 설정 내용을 JSON 문자열로 변환합니다.
        string settingDataJsonString = JsonConvert.SerializeObject(settingData);
        File.WriteAllText(Constants.LOCALDATA_PATH + Constants.SETTINGDATA_FILENAME, settingDataJsonString);


        RefreshRate refreshRate = new();
        refreshRate.numerator = settingData.numerator;
        refreshRate.denominator = settingData.denominator;

        Screen.SetResolution(
            settingData.width, 
            settingData.height, 
            settingData.screenMode, 
            refreshRate);
    }

    private void OnDestroy()
    {
        // 캐릭터 데이터 경로
        string characterDataPath = Constants.LOCALDATA_PATH + Constants.CHARACTERDATA_FILENAME;

        // 캐릭터 데이터를 JSON 문자열로 변환합니다.
        string characterDataToJson = JsonConvert.SerializeObject(characterData);

        // 파일을 생성합니다.
        File.WriteAllText(characterDataPath, characterDataToJson);
    }

}
