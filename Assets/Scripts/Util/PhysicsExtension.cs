using System.Collections.Generic;
using UnityEngine;

public class DrawGizmoLineInfo
{
    public Color defaultColor { get; set; } = new Color(0.0f, 1.0f, 0.0f, 1.0f);
    public Color detectedColor { get; set; } = new Color(1.0f, 0.0f, 0.0f, 1.0f);
    public bool isHit { get; set; }
    public Vector3 start { get; set; }
    public Vector3 end { get; set; }
}

public class DrawGizmoSphereInfo : DrawGizmoLineInfo
{
    public float radius { get; set; }
}

public class DrawGizmoSpheresInfo : DrawGizmoSphereInfo
{
    public List<Vector3> hitPoints;
}



public static class PhysicsExt
{
    public static bool RayCast(
        out DrawGizmoLineInfo info,
        Ray ray,
        out RaycastHit hitInfo,
        float maxDistance,
        int layerMask,
        QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal)
    {
        bool isHit = Physics.Raycast(
            ray,
            out hitInfo,
            maxDistance,
            layerMask,
            queryTriggerInteraction);

        info = new DrawGizmoLineInfo()
        {
            isHit = isHit,
            start = ray.origin,
            end = isHit ? 
                hitInfo.point : // 감지한 위치
                ray.origin + ray.direction * maxDistance // 끝 위치
        };

        return isHit;
    }

    public static bool SphereCast(
        out DrawGizmoSphereInfo info,
        Ray ray, 
        float radius, 
        out RaycastHit hitInfo, 
        float maxDistance, 
        int layerMask, 
        QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal)
    {
        bool isHit = Physics.SphereCast(
            ray,
            radius,
            out hitInfo,
            maxDistance,
            layerMask,
            queryTriggerInteraction);

        info = new DrawGizmoSphereInfo()
        { 
            isHit = isHit,
            start = ray.origin,
            end = isHit ? 
                ray.origin + ray.direction * hitInfo.distance :
                ray.origin + ray.direction * maxDistance,
            radius = radius
        };

        return isHit;
    }

    public static bool SphereCastAll(
        out DrawGizmoSpheresInfo info,
        Ray ray,
        out RaycastHit[] hitResults,
        float radius, 
        float maxDistance, 
        int layerMask, 
        QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal)
    {

        hitResults = Physics.SphereCastAll(
            ray,
            radius,
            maxDistance,
            layerMask,
            queryTriggerInteraction);

        bool isHit = (hitResults == null) ? false : (hitResults.Length > 0);

        List<Vector3> hitPoints = new();
        foreach(RaycastHit hit in hitResults)
        {
            hitPoints.Add(hit.point);
        }

        info = new DrawGizmoSpheresInfo()
        {
            isHit = isHit,
            start = ray.origin,
            end = ray.origin + ray.direction * maxDistance,
            radius = radius,
            hitPoints = hitPoints
        };

        return isHit;
    }

    public static Collider[] OverlapSphere(
        out DrawGizmoSphereInfo info,
        Vector3 center, float radius, int layer, 
        QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal)
    {
        Collider[] detectCollisions = Physics.OverlapSphere(
            center,  radius,  layer,  queryTriggerInteraction);

        bool isDetected = (detectCollisions.Length > 0);

        info = new DrawGizmoSphereInfo()
        {
            isHit = isDetected,
            start = center,
            end = center
        };
        info.radius = radius;

        return detectCollisions;
    }

    public static void DrawGizmoLine(in DrawGizmoLineInfo info)
    {
        if (info == null) return;
        Gizmos.color = info.isHit ? info.detectedColor : info.defaultColor;
        Gizmos.DrawLine(info.start, info.end);
    }

    public static void DrawGizmoSphere(in DrawGizmoSphereInfo info)
    {
        if (info == null) return;
        Gizmos.color = info.defaultColor;
        Gizmos.DrawWireSphere(info.start, info.radius);

        Gizmos.color = info.isHit ? info.detectedColor : info.defaultColor;
        Gizmos.DrawLine(info.start, info.end);
        Gizmos.DrawWireSphere(info.end, info.radius);
    }

    public static void DrawGizmoSpheres(in DrawGizmoSpheresInfo info)
    {
        if (info == null) return;

        // 시작 위치를 그립니다.
        Gizmos.color = info.defaultColor;
        Gizmos.DrawWireSphere(info.start, info.radius);

        Gizmos.color = info.isHit ? info.detectedColor : info.defaultColor;

        // 끝 위치까지 그립니다.
        Gizmos.DrawLine(info.start, info.end);
        Gizmos.DrawWireSphere(info.end, info.radius);

        // 감지된 위치들을 그립니다.
        foreach(Vector3 hitPoint in info.hitPoints)
        {
            Gizmos.DrawWireSphere(hitPoint, info.radius);
        }
    }

    public static void DrawOverlapSphere(in DrawGizmoSphereInfo info)
    {
        Gizmos.color = info.isHit ? info.detectedColor : info.defaultColor;
        Gizmos.DrawWireSphere(info.start, info.radius);
    }

}