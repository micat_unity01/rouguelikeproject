

using UnityEngine;

public static class Constants
{

    /// <summary>
    /// PlayerInput 컴포넌트의 액션맵 중 게임 모드를 나타냅니다.
    /// </summary>
    public const string ACTIONMAP_GAMEPLAY_MODE = "GameInput";

    /// <summary>
    /// PlayerInput 컴포넌트의 액션맵 중 UI 모드를 나타냅니다.
    /// </summary>
    public const string ACTIONMAP_UI_MODE = "UIInput";

    #region 오브젝트 태그
    public const string TAG_PLAYER = "Player";
    #endregion

    #region 플레이어 캐릭터 애니메이터 파라미터 이름
    public const string ANIMPARAM_MOVESPEED     = "_MoveSpeed";
    public const string ANIMPARAM_ISGROUNDED    = "_IsGrounded";
    public const string ANIMPARAM_STARTEQUIPWEAPON = "_StartEquipWeapon";
    public const string ANIMPARAM_STARTUNEQUIPWEAPON = "_StartUnEquipWeapon";
    public const string ANIMPARAM_CURRENTCOMBO = "_CurrentCombo";
    public const string ANIMPARAM_ISBLOCKED = "_IsBlocked";
    public const string ANIMPARAM_ATTACKFINISHED = "_AttackFinished";
    public const string ANIMPARAM_ISHIT = "_IsHit";
    #endregion

    #region 무기 장착, 해제 영역 태그
    public const string AREA_TAG_EQUIPWEAPONZONE = "EquipWeaponZone";
    public const string AREA_TAG_UNEQUIPWEAPONZONE = "UnEquipWeaponZone";
    #endregion

    #region 공격 이름
    public const string ATTACKNAME_DEFAULT = "DefaultAttack";
    #endregion

    #region 오브젝트 레이어
    public const string LAYERNAME_ENEMY = "EnemyCharacter";
    public const string LAYERNAME_PLAYER = "PlayerCharacter";
    public const string LAYERNAME_RAGDOLL = "Ragdoll";
    #endregion

    #region HUD 관련
    public const float ATTACKRESULT_LIFETIMESECONDS = 1.5f;
    #endregion

    #region 월요일 아침 적 캐릭터 애니메이터 파라미터 이름
    public const string ANIMPARAM_MONDAYMORNING_MOVESPEED = "_Speed";
    #endregion

    #region 로컬 데이터 경로
    public static string LOCALDATA_PATH => $"{Application.dataPath}/LocalData/";
    public const string SETTINGDATA_FILENAME = "SettingData.json";
    public const string CHARACTERDATA_FILENAME = "CharacterData.json";
    #endregion



}
